//
//  AboutViewController.swift
//  BullsEye
//
//  Created by Mac10 on 25.02.2019.
//  Copyright © 2019 Mac10. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    @IBOutlet weak var webView : UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let url = Bundle.main.url(forResource: "BullsEye", withExtension: "html"){
            if let htmlData = try?Data(contentsOf: url){
                let baseUrl = URL(fileURLWithPath: Bundle.main.bundlePath)
                webView.load(htmlData, mimeType: "text/html", textEncodingName: "UTF-8", baseURL: baseUrl)
            }
        }
 
        
        /*
         
         if let url = Bundle.main.url(forResource: "BullsEye", withExtension: "html") {
         webView.loadRequest(URLRequest(url: url))
         }else {
         print("load error")
         }
         
         */
     
    }
    
    @IBAction func close(){
        dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
